// +build integration

// To run integration tests use: go test -v -tags=integration gitlab.com/sibex/sibex-go-api-client

package api

import (
	"math/big"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type APITestSuite struct {
	suite.Suite
	api *API
}

func (ts *APITestSuite) SetupSuite() {
	var err error

	cfg := Config{
		DaemonURI: "http://localhost:20222",
		Debug:     true,
		User:      "sibex",
		Password:  "12345678",
	}
	ts.api, err = NewAPI(cfg)
	if err != nil {
		panic(err)
	}
}

func (ts *APITestSuite) SetupTest() {
}

func (ts *APITestSuite) TearDownSuite() {
}

func TestAPITestSuite(t *testing.T) {
	suite.Run(t, new(APITestSuite))
}

func (ts *APITestSuite) TestBtc() {
	expected := Balance{
		Currency: Btc,
		Address:  "*",
		Balance:  &BigInt{big.NewInt(16891815)},
	}
	actual, err := ts.api.GetBalance(Btc)
	assert.Nil(ts.T(), err)
	require.NotNil(ts.T(), actual)
	assert.Equal(ts.T(), expected, *actual)
}

func (ts *APITestSuite) TestEth() {
	b := big.NewInt(0)
	b, ok := b.SetString("18750000000000000000", 10)
	assert.True(ts.T(), ok)

	expected := Balance{
		Currency: Eth,
		Address:  "0x8a7a700f834304e9b390fc2be3fd038abc1eab6e",
		Balance:  &BigInt{b},
	}
	actual, err := ts.api.GetBalance(Eth)
	assert.Nil(ts.T(), err)
	require.NotNil(ts.T(), actual)
	assert.Equal(ts.T(), expected, *actual)
}

func (ts *APITestSuite) TestUSDT() {
	b := big.NewInt(0)
	b, ok := b.SetString("560133890", 10)
	assert.True(ts.T(), ok)

	expected := Balance{
		Currency: USDT,
		Address:  "0x8a7a700f834304e9b390fc2be3fd038abc1eab6e",
		Balance:  &BigInt{b},
	}
	actual, err := ts.api.GetBalance(USDT)
	assert.Nil(ts.T(), err)
	require.NotNil(ts.T(), actual)
	assert.Equal(ts.T(), expected, *actual)
}

func (ts *APITestSuite) TestGetLastPriceWithBtcEth() {
	r := big.NewRat(1, 5000000000)

	pr := Pair{Base: Btc, Quote: Eth}
	t, err := time.Parse(time.RFC3339Nano, "2020-01-10T00:59:29.366Z")
	expected := Price{
		Pair:  pr,
		Price: &BigRat{r},
		Time:  t,
	}
	actual, err := ts.api.GetLastPrice(pr)
	assert.Nil(ts.T(), err, err)
	require.NotNil(ts.T(), actual)
	assert.Equal(ts.T(), expected, *actual)
}

func (ts *APITestSuite) TestGetLastPriceWithEthBtc() {
	r := big.NewRat(5000000000, 1)

	pr := Pair{Base: Eth, Quote: Btc}
	t, err := time.Parse(time.RFC3339Nano, "2020-01-10T00:59:29.366Z")
	expected := Price{
		Pair:  pr,
		Price: &BigRat{r},
		Time:  t,
	}
	actual, err := ts.api.GetLastPrice(pr)
	assert.Nil(ts.T(), err, err)
	require.NotNil(ts.T(), actual)
	assert.Equal(ts.T(), expected, *actual)
}

func (ts *APITestSuite) TestGetLastPriceWithNoBook() {
	pr := Pair{Base: Btc, Quote: USDT}
	actual, err := ts.api.GetLastPrice(pr)
	assert.Nil(ts.T(), actual)
	assert.Nil(ts.T(), err)
}

func (ts *APITestSuite) TestBtcAvailableBalance() {
	expected := big.NewInt(16891815)
	actual, err := ts.api.GetAvailableBalance(Btc)
	assert.Nil(ts.T(), err)
	require.NotNil(ts.T(), actual)
	assert.Equal(ts.T(), &BigInt{expected}, actual)
}

func (ts *APITestSuite) TestUSDTAvailableBalance() {
	expected := big.NewInt(560133890)
	actual, err := ts.api.GetAvailableBalance(USDT)
	assert.Nil(ts.T(), err)
	require.NotNil(ts.T(), actual)
	assert.Equal(ts.T(), &BigInt{expected}, actual)
}

func (ts *APITestSuite) TestEthAvailableBalance() {
	expected := big.NewInt(560133890)
	expected, ok := expected.SetString("18750000000000000000", 10)
	assert.True(ts.T(), ok)
	actual, err := ts.api.GetAvailableBalance(Eth)
	assert.Nil(ts.T(), err)
	require.NotNil(ts.T(), actual)
	assert.Equal(ts.T(), &BigInt{expected}, actual)
}

func (ts *APITestSuite) TestGetOrders() {
	pr := Pair{Base: Btc, Quote: Eth}
	end := time.Now().UTC()

	// expected bids
	ebs := []Order{
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(200000000)},
			QuoteQ:    &BigInt{big.NewInt(200000000)},
			BigPrice:  &BigRat{big.NewRat(200000000, 200000000)},
			Time:      end,
		},
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(20000000)},
			QuoteQ:    &BigInt{big.NewInt(20000000)},
			BigPrice:  &BigRat{big.NewRat(20000000, 20000000)},
			Time:      end,
		},
	}
	// "price": "1200000000000000000/200000000"
	_, ok := ebs[0].QuoteQ.SetString("1200000000000000000", 10)
	assert.True(ts.T(), ok)
	_, ok = ebs[0].BigPrice.SetString("200000000/1200000000000000000")
	assert.True(ts.T(), ok)
	// "price": "1200000000000000000/20000000"
	_, ok = ebs[1].QuoteQ.SetString("1200000000000000000", 10)
	assert.True(ts.T(), ok)
	_, ok = ebs[1].BigPrice.SetString("20000000/1200000000000000000")
	assert.True(ts.T(), ok)

	var eas []Order

	bs, as, err := ts.api.GetOrders(pr, end, 5)
	assert.Nil(ts.T(), err)
	assert.Equal(ts.T(), ebs, bs)
	assert.Equal(ts.T(), eas, as)
}

func (ts *APITestSuite) TestGetOrdersInverted() {
	pr := Pair{Base: Eth, Quote: Btc}
	end := time.Now().UTC()

	// expected bids
	ebs := []Order{
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(200000000)},
			QuoteQ:    &BigInt{big.NewInt(200000000)},
			BigPrice:  &BigRat{big.NewRat(200000000, 200000000)},
			Time:      end,
		},
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(20000000)},
			QuoteQ:    &BigInt{big.NewInt(20000000)},
			BigPrice:  &BigRat{big.NewRat(20000000, 20000000)},
			Time:      end,
		},
	}
	// "price": "1200000000000000000/200000000"
	_, ok := ebs[0].BaseQ.SetString("1200000000000000000", 10)
	assert.True(ts.T(), ok)
	_, ok = ebs[0].BigPrice.SetString("1200000000000000000/200000000")
	assert.True(ts.T(), ok)
	// "price": "1200000000000000000/20000000"
	_, ok = ebs[1].BaseQ.SetString("1200000000000000000", 10)
	assert.True(ts.T(), ok)
	_, ok = ebs[1].BigPrice.SetString("1200000000000000000/20000000")
	assert.True(ts.T(), ok)

	var eas []Order

	bs, as, err := ts.api.GetOrders(pr, end, 5)
	assert.Nil(ts.T(), err)
	assert.Equal(ts.T(), ebs, bs)
	assert.Equal(ts.T(), eas, as)
}

func (ts *APITestSuite) TestPlaceOrder() {
	pr := Pair{Base: Eth, Quote: Btc}
	t := time.Now().UTC()
	// order expires in 60 seconds
	vu := t.Add(time.Second * 60)
	o := Order{
		Pair:      pr,
		OrderType: Bid,
		BaseQ:     &BigInt{big.NewInt(2)},
		QuoteQ:    &BigInt{big.NewInt(3)},
		BigPrice:  &BigRat{big.NewRat(2, 3)},
		Time:      t,
		ExpiresAt: vu,
	}
	_, ok := o.BaseQ.SetString("7222333300001111111", 10)
	assert.True(ts.T(), ok)
	_, ok = o.QuoteQ.SetString("1220000", 10)
	assert.True(ts.T(), ok)
	_, ok = o.BigPrice.SetString("7222333300001111111/1220000")
	assert.True(ts.T(), ok)

	assert.Equal(ts.T(), "", o.EID)
	// run method under test
	err := ts.api.PlaceOrder(&o)
	assert.Nil(ts.T(), err)
	assert.Contains(ts.T(), o.EID, "orderId_")
	assert.Equal(ts.T(), Placed, o.OrderStatus)
}

func (ts *APITestSuite) TestPlaceOrderError() {
	pr := Pair{Base: Eth, Quote: Btc}
	t := time.Now().UTC()
	// order expires in 60 seconds
	vu := t.Add(time.Second * 60)
	o := Order{
		Pair:      pr,
		OrderType: Bid,
		BaseQ:     &BigInt{big.NewInt(2)},
		QuoteQ:    &BigInt{big.NewInt(3)},
		BigPrice:  &BigRat{big.NewRat(2, 3)},
		Time:      t,
		ExpiresAt: vu,
	}
	// run method under test
	err := ts.api.PlaceOrder(&o)
	assert.NotNil(ts.T(), err)
	assert.Equal(ts.T(), "server failure, ETH Value too low", err.Error())
	assert.NotEqual(ts.T(), Placed, o.OrderStatus)
}

func (ts *APITestSuite) TestPlaceOrderInverted() {
	pr := Pair{Base: Btc, Quote: Eth}
	t := time.Now().UTC()
	// order expires in 60 seconds
	vu := t.Add(time.Second * 60)
	o := Order{
		Pair:      pr,
		OrderType: Bid,
		BaseQ:     &BigInt{big.NewInt(2)},
		QuoteQ:    &BigInt{big.NewInt(3)},
		BigPrice:  &BigRat{big.NewRat(2, 3)},
		Time:      t,
		ExpiresAt: vu,
	}
	_, ok := o.BaseQ.SetString("20000099", 10)
	assert.True(ts.T(), ok)
	_, ok = o.QuoteQ.SetString("622222222223300000066", 10)
	assert.True(ts.T(), ok)
	_, ok = o.BigPrice.SetString("20000099/622222222223300000066")
	assert.True(ts.T(), ok)

	assert.Equal(ts.T(), "", o.EID)
	// run method under test
	err := ts.api.PlaceOrder(&o)
	assert.Nil(ts.T(), err)
	assert.Contains(ts.T(), o.EID, "orderId_")
	assert.Equal(ts.T(), Placed, o.OrderStatus)
}

func (ts *APITestSuite) TestCancelOrder() {
	pr := Pair{Base: Eth, Quote: Btc}
	t := time.Now().UTC()
	// order expires in 10 seconds
	vu := t.Add(time.Second * 10)
	o := Order{
		Pair:      pr,
		OrderType: Bid,
		BaseQ:     &BigInt{big.NewInt(2)},
		QuoteQ:    &BigInt{big.NewInt(3)},
		BigPrice:  &BigRat{big.NewRat(2, 3)},
		Time:      t,
		ExpiresAt: vu,
	}
	_, ok := o.BaseQ.SetString("5000000000000000000", 10)
	assert.True(ts.T(), ok)
	_, ok = o.QuoteQ.SetString("1000000000000", 10)
	assert.True(ts.T(), ok)
	_, ok = o.BigPrice.SetString("5000000000000000000/1000000000000")
	assert.True(ts.T(), ok)

	assert.Equal(ts.T(), "", o.EID)
	err := ts.api.PlaceOrder(&o)
	assert.Nil(ts.T(), err)
	assert.Contains(ts.T(), o.EID, "orderId_")

	// run method under test
	err = ts.api.CancelOrder(o.EID)
	assert.Nil(ts.T(), err)
}

func (ts *APITestSuite) TestCancelOrderThatDoesNotExist() {
	// run method under test
	err := ts.api.CancelOrder("this-order-does-not-exist")
	require.NotNil(ts.T(), err)
	assert.Equal(ts.T(), "server failure, Order with orderId this-order-does-not-exist couldn't be deleted", err.Error())
}

func (ts *APITestSuite) TestGetMyOrdersInverted() {
	pr := Pair{Base: Btc, Quote: Eth}
	end := time.Now().UTC()

	// expected bids
	cd, err := time.Parse(time.RFC3339, "2020-01-15T07:04:23.968863899Z")
	assert.Nil(ts.T(), err)
	vu, err := time.Parse(time.RFC3339, "2020-01-15T07:04:33.913017819Z")
	assert.Nil(ts.T(), err)

	var ebs []Order

	// expected asks
	cd, err = time.Parse(time.RFC3339, "2020-01-15T07:04:24.023810552Z")
	assert.Nil(ts.T(), err)
	vu, err = time.Parse(time.RFC3339, "2020-01-15T07:04:33.986925146Z")
	assert.Nil(ts.T(), err)
	eas := []Order{
		{
			Pair:        pr,
			OrderType:   Bid,
			BaseQ:       &BigInt{big.NewInt(1)},
			QuoteQ:      &BigInt{big.NewInt(2)},
			BigPrice:    &BigRat{big.NewRat(1, 2)},
			Time:        cd,
			ExpiresAt:   vu,
			EID:         "orderId_b5cf56d0-e711-4635-896d-e50adae32467",
			OrderStatus: Placed,
		},
	}
	_, ok := eas[0].BaseQ.SetString("9000000000099", 10)
	assert.True(ts.T(), ok)
	_, ok = eas[0].QuoteQ.SetString("6000000000000000066", 10)
	assert.True(ts.T(), ok)
	_, ok = eas[0].BigPrice.SetString("9000000000099/6000000000000000066")
	assert.True(ts.T(), ok)

	bs, as, err := ts.api.GetMyOrders(pr, end, 60)
	assert.Nil(ts.T(), err)
	assert.Equal(ts.T(), ebs, bs)
	assert.Equal(ts.T(), eas, as)
}

func (ts *APITestSuite) TestGetSwaps() {
	// run method under test
	ss, err := ts.api.GetSwaps()
	assert.Nil(ts.T(), err)
	assert.Equal(ts.T(), []Swap{}, ss)
}

func (ts *APITestSuite) TestGetVersionInfo() {
	// call provider for test
	vi, err := ts.api.GetAPIVersion()
	assert.Nil(ts.T(), err)
	assert.Equal(ts.T(), true, vi.Success)
}
