# Go library for the SIBEX API

Hello! This is the [go](https://golang.org/) library for the [SIBEX REST API](https://docs.test.sibex.io/api).

It is still very much work in progress and growing fast.
We welcome both issues and merge requests.

Have fun with it :)

## Examples

Here's how to run the [example](examples/lastprice.go):

```bash
AUTH_API_USER=<user> AUTH_API_PASS=<pwd> DAEMON_HOST=<host> DAEMON_PORT=<port> go run examples/lastprice.go
```

If you are running a sibex client on your local machine use the same user and password and the host and port should be `localhost` and `20222` respectively.

## Implemented request

|       Request name      	|    Status   	|
|:-----------------------:	|:-----------:	|
|       GetBalanceV2      	| Implemented 	|
|     BackupBTCWallet     	| Implemented 	|
|      GetBTCBalance      	| Implemented 	|
| GetBTCAddressForDeposit 	| Implemented 	|
|        GetBTCFee        	| Implemented 	|
|        GetBTCInfo       	| Implemented 	|
|   GetBTCTradingEquity   	| Implemented 	|
|       WithdrawBTC       	| Implemented 	|
|  CommserverCancelOrder  	| Implemented 	|
|    GetCommserverInfo    	| Implemented 	|
|   SetCommserverLicense  	| Implemented 	|
|   GetCommserverMarket   	| Implemented 	|