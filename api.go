// Package api makes the data/operations of the SIBEX REST API accessible to go
// developers.
package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"strings"
	"time"
)

const
(
	defaultClientTimeout = 10

	getVersionEndpoint = "%s/api/v1/balance"
)

// Currency represent the currencies supported by the SIBEX API.
type Currency int

const (
	// Btc - bitcoin
	Btc Currency = iota + 1
	// Eth - ether
	Eth
	// USDT - tether
	USDT
	// USD - US Dollar
	USD
)

func (c Currency) String() string {
	var r string
	switch c {
	case Btc:
		r = "btc"
	case Eth:
		r = "eth"
	case USDT:
		r = "usdt"
	case USD:
		r = "usd"
	}
	return r
}

// Denomination returns the number of base units in a currency.
func (c Currency) Denomination() int64 {
	var r int64
	switch c {
	case Btc:
		r = 1e8
	case Eth:
		r = 1e18
	case USDT:
		r = 1e6
	case USD:
		r = 1e2
	}
	return r
}

// Precision returns the number of digits after the floating point for a
// currency.
func (c Currency) Precision() int32 {
	var r int32
	switch c {
	case Btc:
		r = 8
	case Eth:
		r = 18
	case USDT:
		r = 6
	case USD:
		r = 2
	}
	return r
}

// IsValid returns `true` if the Currency is valid
func (c Currency) IsValid() bool {
	return c >= Btc && c <= USD
}

// Config keeps all (configuration) data needed by the API functions
type Config struct {
	DaemonURI string
	User      string
	Password  string
	Timeout   time.Duration
	Debug     bool
}

// API implements the SIBEX go client API
type API struct {
	Config
}

// NewAPI constructs a new API struct
func NewAPI(c Config) (*API, error) {

	// this will show additional fields such line and time while logging
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	a := &API{
		Config: c,
	}
	if a.Timeout == 0 {
		a.Timeout = defaultClientTimeout
	}
	return a, nil
}

// Balance represents the balance for a particular currency
type Balance struct {
	ID        int       `json:"-" db:"id"`
	Currency  Currency  `json:"currency" db:"currency"`
	Address   string    `json:"address" db:"address"`
	Balance   *BigInt   `json:"balance" db:"balance"`
	Available bool      `json:"available" db:"available"`
	Time      time.Time `json:"time" db:"time"`
}

func (b Balance) String() string {
	json, _ := json.MarshalIndent(b, "", "  ")
	return string(json)
}

// Pair represents a trading pair
type Pair struct {
	Base  Currency `json:"base" db:"base"`
	Quote Currency `json:"quote" db:"quote"`
}

// IsValid returns `true` if the Pair is valid
func (pr Pair) IsValid() bool {
	return pr.Base.IsValid() && pr.Quote.IsValid()
}

func (pr Pair) String() string {
	json, _ := json.MarshalIndent(pr, "", "  ")
	return string(json)
}

// Price represents a particular price
type Price struct {
	ID int `json:"-" db:"id"`
	Pair
	Quantities *BigRat   `json:"quantities" db:"quantities"`
	Time       time.Time `json:"time" db:"time"`
}

func (p Price) String() string {
	json, _ := json.MarshalIndent(p, "", "  ")
	return string(json)
}

// IsValid returns `true` if the Price is valid
func (p Price) IsValid() bool {
	return p.Pair.IsValid() && p.Quantities != nil
}

// OrderType represents an order type and may be one of the following: bid, ask
type OrderType int

const (
	// Bid - a trader wishes to buy
	Bid OrderType = iota + 1
	// Ask - a trader wishes to sell
	Ask
)

func (ot OrderType) String() string {
	var r string
	switch ot {
	case Bid:
		r = "bid"
	case Ask:
		r = "ask"
	}
	return r
}

// IsValid returns `true` if the OrderType is valid
func (ot OrderType) IsValid() bool {
	return ot == Bid || ot == Ask
}

// OrderStatus represents the status of an order
type OrderStatus int

const (
	// Created - a new order
	Created OrderStatus = iota + 1
	// Placed - the order was pushed to the comm server
	Placed
	// Expired - the order has expired
	Expired
	// Complete - the order was filled
	Complete
	// Canceled - the order was canceled
	Canceled
	// Failed - the order failed
	Failed
	// Partial - the order was partially filled
	Partial
)

func (os OrderStatus) String() string {
	var r string
	switch os {
	case Created:
		r = "created"
	case Placed:
		r = "placed"
	case Expired:
		r = "expired"
	case Complete:
		r = "complete"
	case Canceled:
		r = "canceled"
	case Failed:
		r = "failed"
	case Partial:
		r = "partial fill"
	}
	return r
}

// IsValid returns `true` if the OrderStatus is valid
func (os OrderStatus) IsValid() bool {
	return os >= Created && os <= Partial
}

// Order represents a particular order
type Order struct {
	ID int `json:"-" db:"id"`
	Pair
	// order type
	OrderType OrderType `json:"order_type" db:"order_type"`
	// order status
	OrderStatus OrderStatus `json:"order_status" db:"order_status"`
	// the quantity of the order that was filled ("price" in SIBEX jargon)
	FilledQ string `json:"filledq" db:"filledq"`
	// base quantity
	BaseQ *BigInt `json:"baseq" db:"baseq"`
	// quote quantity
	QuoteQ *BigInt `json:"quoteq" db:"quoteq"`
	// Quantities (authoritative price, underpinned by a `big.Rat`)
	Quantities *BigRat `json:"quantities" db:"quantities"`
	// Price (stored as a `float64`, ignored by the API, use at own peril)
	Price float64 `json:"price" db:"price"`
	// external ID
	EID string `json:"eid" db:"eid"`
	// valid until
	ExpiresAt time.Time `json:"expires_at" db:"expires_at"`
	// Creation time
	Time time.Time `json:"time" db:"time"`
}

func (o Order) String() string {
	json, _ := json.MarshalIndent(o, "", "  ")
	return string(json)
}

// IsValid returns `true` if the Order is valid
func (o Order) IsValid() bool {
	return o.Pair.IsValid() && o.OrderType.IsValid() && o.BaseQ != nil && o.QuoteQ != nil && o.Quantities != nil
}

// Swap captures swap status data.
type Swap struct {
	ID             int       `json:"-" db:"id"`
	Progress       float64   `json:"progress" db:"progress"`
	Pair           string    `json:"pair" db:"pair"`
	Time           time.Time `json:"time" db:"time"`
	Status         string    `json:"status" db:"status"`
	Quantities     string    `json:"quantities" db:"quantities"`
	Participant    bool      `json:"participant" db:"participant"`
	Action         string    `json:"action" db:"action"`
	OrderID        string    `json:"id" db:"order_id"`
	BConfirmations int       `json:"leadCurrencyConfirmations" db:"bconfirmations"`
	QConfirmations int       `json:"oppositeCurrencyConfirmations" db:"qconfirmations"`
}

// Version represents the daemon's balance
type Version struct {
	Build   string `json:"build"`
	Version string `json:"balance"`
}

func (o Version) String() string {
	json, _ := json.MarshalIndent(o, "", "  ")
	return string(json)
}

// GetBalance returns the balance for the given currency or an error
func (a API) GetBalance(cy Currency) (*Balance, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}
	var uri, ps string

	if !cy.IsValid() {
		return nil, fmt.Errorf("invalid currency: '%v'", cy)
	}
	if cy == USDT {
		uri = fmt.Sprintf("%s/api/v1/erc20/balance", a.DaemonURI)
		ps = `{"token": "usdt"}`
	} else {
		uri = fmt.Sprintf("%s/api/v1/%s/balance", a.DaemonURI, cy.String())
	}

	if a.Debug {
		log.Printf("uri = '%s'", uri)
		log.Printf("params = '%s'", ps)
	}
	req, err := http.NewRequest(http.MethodGet, uri, strings.NewReader(ps))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to get balance for %s: %v", cy, err)
		log.Print(err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseGetBalance(cy, bbs)
}

// parseGetBalance parses the json that came back in the request body
//
// {
//   "success": true,
//   "data": {
//     "address": "0x8a7a700f834304e9b390fc2be3fd038abc1eab6e",
//     "balance": 560133890
//   },
//   "balance": "1"
// }
func parseGetBalance(cy Currency, body []byte) (*Balance, error) {
	type container struct {
		Success bool     `json:"success"`
		B       *Balance `json:"data"`
		Error   string   `json:"error"`
	}
	var cr = container{B: &Balance{Balance: &BigInt{big.NewInt(0)}}}

	// parse the body as json
	err := json.Unmarshal(body, &cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return nil, err
	}

	cr.B.Currency = cy
	cr.B.Time = time.Now().UTC()
	return cr.B, err
}

// GetLastPrice returns the last price for the given pair (if any)
func (a API) GetLastPrice(pr Pair) (*Price, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}
	var uri, ps string

	if !pr.IsValid() {
		return nil, fmt.Errorf("invalid currency pair: '%v'", pr)
	}

	// prepare the request
	uri = fmt.Sprintf("%s/api/v1/commserver/price/history", a.DaemonURI)
	ps = fmt.Sprintf(`{"currency1": "%s", "currency2": "%s"}`, pr.Base, pr.Quote)

	if a.Debug {
		log.Printf("uri = '%s'", uri)
		log.Printf("params = '%s'", ps)
	}
	req, err := http.NewRequest(http.MethodGet, uri, strings.NewReader(ps))
	if err != nil {
		log.Print(err)
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	// make the request
	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to get price history for %s: %v", pr, err)
		log.Print(err)
		return nil, err
	}
	defer resp.Body.Close()

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	// please note: we will get a status of 400 (bad request) if
	// "No book found for this pair"
	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusBadRequest {
			if strings.Index(string(bbs), "No book found for this pair") > -1 {
				return nil, nil
			}
		}
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, err
	}

	return parseGetLastPrice(pr, bbs)
}

// parseGetLastPrice parses the json returned by the request
//
// {
//   "success": true,
//   "data": {
//     "Pair": "ETH/BTC",
//     "prices": [
//       {
//         "price": "1400000000000000000/280000000",
//         "time": "2020-01-10T00:59:29.366Z"
//       },
//       {
//         "price": "1300000000000000000/260000000",
//         "time": "2020-01-09T13:19:22.231Z"
//       },
//       {
//         "price": "100000000000000000/20000000",
//         "time": "2020-01-06T11:04:36.746Z"
//       }
//     ]
//   },
//   "balance": "1"
// }
func parseGetLastPrice(pr Pair, body []byte) (*Price, error) {
	// define the structs we need to parse the json
	type price struct {
		Price string    `json:"price"`
		Time  time.Time `json:"time"`
	}
	type data struct {
		Pair   string  `json:"pair"`
		Prices []price `json:"prices"`
	}
	type container struct {
		Success bool   `json:"success"`
		Data    data   `json:"data"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	err := json.Unmarshal(body, &cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return nil, err
	}

	// no trades
	if len(cr.Data.Prices) == 0 {
		return nil, nil
	}

	// prepare result (most recent trade)
	br := big.NewRat(1, 1)
	_, ok := br.SetString(cr.Data.Prices[0].Price)
	if !ok {
		log.Printf("invalid price value: '%v'", cr.Data.Prices[0].Price)
		return nil, err
	}

	res := &Price{
		Pair:       pr,
		Quantities: &BigRat{br},
		Time:       cr.Data.Prices[0].Time,
	}
	// does the returned pair start with the quote currency?
	if !strings.HasPrefix(strings.ToLower(cr.Data.Pair), pr.Base.String()) {
		// yes, invert price
		res.Quantities = &BigRat{Rat: res.Quantities.Inv(res.Quantities.Rat)}
	}
	return res, nil
}

// GetAvailableBalance returns the balance minus equity of open orders for the
// given currency.
func (a API) GetAvailableBalance(cy Currency) (*Balance, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}
	var uri, ps string

	if !cy.IsValid() {
		return nil, fmt.Errorf("invalid currency: '%v'", cy)
	}

	if cy == USDT {
		uri = fmt.Sprintf("%s/api/v1/erc20/tradingequity", a.DaemonURI)
		ps = `{"token": "usdt"}`
	} else {
		uri = fmt.Sprintf("%s/api/v1/%s/tradingequity", a.DaemonURI, cy.String())
	}

	if a.Debug {
		log.Printf("uri = '%s'", uri)
		log.Printf("params = '%s'", ps)
	}
	req, err := http.NewRequest(http.MethodGet, uri, strings.NewReader(ps))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to get balance for %s: %v", cy, err)
		log.Print(err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseGetAvailableBalance(cy, bbs)
}

// parseGetAvailableBalance parses the json that came back in the request body
//
// {
//   "success": true,
//   "data": 560133890,
//   "balance": "1"
// }
func parseGetAvailableBalance(cy Currency, body []byte) (*Balance, error) {
	type container struct {
		Success bool    `json:"success"`
		B       *BigInt `json:"data"`
		Error   string  `json:"error"`
	}
	cr := container{B: &BigInt{Int: big.NewInt(0)}}

	// parse the body as json
	err := json.Unmarshal(body, &cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return nil, err
	}

	b := &Balance{
		Currency:  cy,
		Available: true,
		Balance:   cr.B,
		Time:      time.Now().UTC(),
	}
	return b, err
}

// GetOrders returns bids / asks for the given pair going back `period` seconds
// from `end`.
func (a API) GetOrders(pr Pair, end time.Time, period uint) ([]Order, []Order, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}
	var uri, ps string

	if !pr.IsValid() {
		return nil, nil, fmt.Errorf("invalid currency pair: '%v'", pr)
	}

	uri = fmt.Sprintf("%s/api/v1/commserver/orders", a.DaemonURI)
	start := end.Add(time.Duration(-period) * time.Second)

	ps = `{
		"currency1": "%s",
		"currency2": "%s",
		"timeRangeStart": "%s",
		"timeRangeEnd": "%s"}`

	ps = fmt.Sprintf(ps, pr.Base, pr.Quote, start.Format(time.RFC3339Nano), end.Format(time.RFC3339Nano))
	if a.Debug {
		log.Printf("uri = '%s'", uri)
		log.Printf("params = '%s'", ps)
	}

	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(ps))
	if err != nil {
		log.Print(err)
		return nil, nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to get balance for %s: %v", pr, err)
		log.Print(err)
		return nil, nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseGetOrders(pr, bbs)
}

// parseGetOrders parses the json payload returned by commserver/orders
func parseGetOrders(pr Pair, body []byte) ([]Order, []Order, error) {
	// define the structs we need to parse the json
	type price struct {
		Price string `json:"price"`
	}
	type data struct {
		Bids   []price   `json:"buyOrders"`
		Asks   []price   `json:"sellOrders"`
		Market string    `json:"market"`
		TS     time.Time `json:"timeRangeEnd"`
	}
	type container struct {
		Success bool   `json:"success"`
		Data    data   `json:"data"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	err := json.Unmarshal(body, &cr)
	if err != nil || !cr.Success {
		log.Print("Failed to decode response body")
		return nil, nil, err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return nil, nil, err
	}

	var invert bool

	if !strings.HasPrefix(strings.ToLower(cr.Data.Market), pr.Base.String()) {
		invert = true
	}
	var bids, asks []Order
	var ot OrderType

	for _, bs := range cr.Data.Bids {
		bq, qq, err := parseQuantities(invert, bs.Price)
		if err != nil {
			return nil, nil, err
		}
		p := big.NewRat(1, 1)
		_, ok := p.SetString(fmt.Sprintf("%s/%s", bq, qq))
		if !ok {
			err := fmt.Errorf("failed to set price string with bq = '%s', qq = '%s'", bq, qq)
			return nil, nil, err
		}
		if invert {
			ot = Ask
		} else {
			ot = Bid
		}
		order := Order{
			Pair:       pr,
			OrderType:  ot,
			BaseQ:      bq,
			QuoteQ:     qq,
			Quantities: &BigRat{Rat: p},
			Time:       cr.Data.TS,
		}
		if invert {
			asks = append(asks, order)
		} else {
			bids = append(bids, order)
		}
	}
	for _, as := range cr.Data.Asks {
		bq, qq, err := parseQuantities(invert, as.Price)
		if err != nil {
			return nil, nil, err
		}
		p := big.NewRat(1, 1)
		_, ok := p.SetString(fmt.Sprintf("%s/%s", bq, qq))
		if !ok {
			err := fmt.Errorf("failed to set price string with bq = '%s', qq = '%s'", bq, qq)
			return nil, nil, err
		}
		if invert {
			ot = Bid
		} else {
			ot = Ask
		}
		order := Order{
			Pair:       pr,
			OrderType:  ot,
			BaseQ:      bq,
			QuoteQ:     qq,
			Quantities: &BigRat{Rat: p},
			Time:       cr.Data.TS,
		}
		if invert {
			bids = append(bids, order)
		} else {
			asks = append(asks, order)
		}
	}
	return bids, asks, nil
}

// parseQuantities parses the "price" returned by commserver/orders and flips
// the quantities if base/quote does not match the "market".
func parseQuantities(invert bool, price string) (*BigInt, *BigInt, error) {
	qs := strings.Split(price, "/")
	if len(qs) != 2 {
		err := fmt.Errorf("invalid price: '%s'", price)
		return nil, nil, err
	}
	var bq, qq *BigInt
	var bqi, qqi = 0, 1

	if invert {
		bqi = 1
		qqi = 0
	}

	// base quantity
	bq = &BigInt{big.NewInt(0)}
	_, ok := bq.SetString(qs[bqi], 10)
	if !ok {
		err := fmt.Errorf("invalid base amount: '%s'", qs[bqi])
		return nil, nil, err
	}
	// quote quantity
	qq = &BigInt{big.NewInt(0)}
	_, ok = qq.SetString(qs[qqi], 10)
	if !ok {
		err := fmt.Errorf("invalid quote amount: '%s'", qs[qqi])
		return nil, nil, err
	}
	return bq, qq, nil
}

// marketString returns the market as well as the "price" quantities in the
// right order
func marketString(o Order) (string, string, error) {
	var market, price string
	price = fmt.Sprintf("%s/%s", o.BaseQ, o.QuoteQ)
	switch o.Base {
	case Btc:
		switch o.Quote {
		case Eth:
			market = "ETH/BTC"
			price = fmt.Sprintf("%s/%s", o.QuoteQ, o.BaseQ)
		case USDT:
			market = "BTC/USDT"
		default:
			return "", "", fmt.Errorf("invalid pair: %s/%s", o.Base, o.Quote)
		}
	case Eth:
		switch o.Quote {
		case Btc:
			market = "ETH/BTC"
		case USDT:
			market = "ETH/USDT"
		default:
			return "", "", fmt.Errorf("invalid pair: %s/%s", o.Base, o.Quote)
		}
	case USDT:
		switch o.Quote {
		case Btc:
			market = "BTC/USDT"
			price = fmt.Sprintf("%s/%s", o.QuoteQ, o.BaseQ)
		case Eth:
			market = "ETH/USDT"
			price = fmt.Sprintf("%s/%s", o.QuoteQ, o.BaseQ)
		default:
			return "", "", fmt.Errorf("invalid pair: %s/%s", o.Base, o.Quote)
		}
	}
	return market, price, nil
}

func orderAction(o Order, invert bool) int {
	switch o.OrderType {
	case Bid:
		if invert {
			return 0
		}
		return 1
	case Ask:
		if invert {
			return 1
		}
		return 0
	}
	// impossible case
	return -1
}

// PlaceOrder places an order and sets the order ID returned by the server in
// the `o` parameter if successful
func (a API) PlaceOrder(o *Order) error {
	if !o.IsValid() {
		return fmt.Errorf("invalid order: '%v'", o)
	}
	psf := `{"market":"%s","orderAction":%d,"price":"%s","dark":false,"validUntil":"%s"}`
	ms, price, err := marketString(*o)
	if err != nil {
		return err
	}
	var invert bool
	if !strings.HasPrefix(strings.ToLower(ms), o.Base.String()) {
		invert = true
	}
	oa := orderAction(*o, invert)
	vu := o.ExpiresAt.Format(time.RFC3339Nano)
	ps := fmt.Sprintf(psf, ms, oa, price, vu)

	uri := fmt.Sprintf("%s/api/v1/commserver/placeorder", a.DaemonURI)
	if a.Debug {
		log.Printf("uri = '%s'", uri)
		log.Printf("params = '%s'", ps)
	}

	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(ps))
	if err != nil {
		return err
	}
	req.SetBasicAuth(a.User, a.Password)
	req.Header.Set("Content-Type", "application/json")

	var c = &http.Client{Timeout: a.Timeout * time.Second}
	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to place order '%v', %v", o, err)
		log.Print(err)
		return err
	}
	defer resp.Body.Close()

	// get the body
	//
	// success:
	// {
	//   "success": true,
	//   "data": {
	//     "isPlacedToCommbook": true,
	//     "orderId": "orderId_dd0db122-9c8a-448d-a097-50d4bdac62e6"
	//   },
	//   "balance": "1"
	// }
	//
	// error:
	// {
	//   "balance": "1",
	//   "success": false,
	//   "error": "ETH Value too low"
	// }

	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	type data struct {
		Placed  bool   `json:"isPlacedToCommbook"`
		OrderID string `json:"orderId"`
	}
	type container struct {
		Success bool   `json:"success"`
		D       data   `json:"data"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	err = json.NewDecoder(bytes.NewReader(bbs)).Decode(&cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return err
	}
	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return err
	}
	if !cr.D.Placed {
		log.Print("server returns: isPlacedToCommbook = false")
	}
	o.EID = cr.D.OrderID
	o.OrderStatus = Placed

	return err
}

// CancelOrder cancels the order with the given order ID
func (a API) CancelOrder(oid string) error {
	ps := fmt.Sprintf(`{"orderId":"%s"}`, oid)

	uri := fmt.Sprintf("%s/api/v1/commserver/cancelorder", a.DaemonURI)
	if a.Debug {
		log.Printf("uri = '%s'", uri)
		log.Printf("params = '%s'", ps)
	}
	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(ps))
	if err != nil {
		log.Print(err)
		return err
	}
	req.SetBasicAuth(a.User, a.Password)
	req.Header.Set("Content-Type", "application/json")

	var c = &http.Client{Timeout: a.Timeout * time.Second}
	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to cancel order '%s', %v", oid, err)
		log.Print(err)
		return err
	}
	defer resp.Body.Close()

	// get the body
	//
	// success:
	// {
	//   "success": true,
	//   "data": {
	//     "orderId": "orderId_5fd859d0-5c72-4136-b5cf-3a3aace13e7d"
	//   },
	//   "balance": "1"
	// }
	//
	// error:
	// {
	//   "balance": "1",
	//   "success": false,
	//   "error": "Order with orderId no-such-order couldn't be deleted"
	// }

	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	type container struct {
		Success bool   `json:"success"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	err = json.NewDecoder(bytes.NewReader(bbs)).Decode(&cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return err
	}

	return err
}

// GetMyOrders returns bids / asks we placed *ourselves* for the given pair
// going back `period` seconds from `end`.
func (a API) GetMyOrders(pr Pair, end time.Time, period uint) ([]Order, []Order, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}
	var uri, ps string

	if !pr.IsValid() {
		return nil, nil, fmt.Errorf("invalid currency pair: '%v'", pr)
	}

	uri = fmt.Sprintf("%s/api/v1/commserver/myorders", a.DaemonURI)
	start := end.Add(time.Duration(-period) * time.Second)

	ps = `{
		"currency1": "%s",
		"currency2": "%s",
		"timeRangeStart": "%s",
		"timeRangeEnd": "%s"}`

	ps = fmt.Sprintf(ps, pr.Base, pr.Quote, start.Format(time.RFC3339Nano), end.Format(time.RFC3339Nano))
	if a.Debug {
		log.Printf("uri = '%s'", uri)
		log.Printf("params = '%s'", ps)
	}

	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(ps))
	if err != nil {
		log.Print(err)
		return nil, nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to get balance for %s: %v", pr, err)
		log.Print(err)
		return nil, nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseMyOrders(pr, bbs)
}

// parseMyOrders parses the json payload returned by commserver/myorders
//
// See `api_test.go` for an example
func parseMyOrders(pr Pair, body []byte) ([]Order, []Order, error) {
	// define the structs we need to parse the json
	type price struct {
		Created    time.Time `json:"created"`
		EID        string    `json:"orderId"`
		Market     string    `json:"orderMarket"`
		Price      string    `json:"price"`
		ValidUntil time.Time `json:"validUntil"`
	}
	type data struct {
		Bids []price `json:"buyOrders"`
		Asks []price `json:"sellOrders"`
	}
	type container struct {
		Success bool   `json:"success"`
		Data    data   `json:"data"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&cr)
	if err != nil || !cr.Success {
		log.Print("Failed to decode response body")
		return nil, nil, err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return nil, nil, err
	}

	var invert bool
	var ot OrderType
	var bids, asks []Order

	for _, bs := range cr.Data.Bids {
		if !strings.HasPrefix(strings.ToLower(bs.Market), pr.Base.String()) {
			invert = true
		}
		bq, qq, err := parseQuantities(invert, bs.Price)
		if err != nil {
			return nil, nil, err
		}
		p := big.NewRat(1, 1)
		_, ok := p.SetString(fmt.Sprintf("%s/%s", bq, qq))
		if !ok {
			err := fmt.Errorf("failed to set price string with bq = '%s', qq = '%s'", bq, qq)
			return nil, nil, err
		}
		if invert {
			ot = Ask
		} else {
			ot = Bid
		}
		order := Order{
			Pair:        pr,
			OrderType:   ot,
			BaseQ:       bq,
			QuoteQ:      qq,
			Quantities:  &BigRat{Rat: p},
			Time:        bs.Created,
			ExpiresAt:   bs.ValidUntil,
			EID:         bs.EID,
			OrderStatus: Placed,
		}
		if invert {
			asks = append(asks, order)
		} else {
			bids = append(bids, order)
		}
	}
	for _, as := range cr.Data.Asks {
		if !strings.HasPrefix(strings.ToLower(as.Market), pr.Base.String()) {
			invert = true
		}
		bq, qq, err := parseQuantities(invert, as.Price)
		if err != nil {
			return nil, nil, err
		}
		p := big.NewRat(1, 1)
		_, ok := p.SetString(fmt.Sprintf("%s/%s", bq, qq))
		if !ok {
			err := fmt.Errorf("failed to set price string with bq = '%s', qq = '%s'", bq, qq)
			return nil, nil, err
		}
		if invert {
			ot = Bid
		} else {
			ot = Ask
		}
		order := Order{
			Pair:        pr,
			OrderType:   ot,
			BaseQ:       bq,
			QuoteQ:      qq,
			Quantities:  &BigRat{Rat: p},
			Time:        as.Created,
			ExpiresAt:   as.ValidUntil,
			EID:         as.EID,
			OrderStatus: Placed,
		}
		if invert {
			bids = append(bids, order)
		} else {
			asks = append(asks, order)
		}
	}
	return bids, asks, nil
}

// ClearSwaps clears the swap status i.e. deletes data pertaining to swaps in
// the past.
func (a API) ClearSwaps() error {
	ps := `{"DryRun": false}`

	// these literals with url better endure to const above (as balance endpoint)
	uri := fmt.Sprintf("%s/api/v1/swapstatus/clear", a.DaemonURI)
	if a.Debug {
		log.Printf("uri = '%s'", uri)
		log.Printf("params = '%s'", ps)
	}
	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(ps))
	if err != nil {
		log.Print(err)
		return err
	}
	req.SetBasicAuth(a.User, a.Password)
	req.Header.Set("Content-Type", "application/json")

	var c = &http.Client{Timeout: a.Timeout * time.Second}
	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to clear swaps, %v", err)
		log.Print(err)
		return err
	}
	defer resp.Body.Close()

	// get the body
	//
	// success:
	// {
	//   "success": true,
	//   "data": {
	//     "deletedCount": 0
	//   },
	//   "balance": "1"
	// }
	//
	// error:
	// {
	//   "balance": "1",
	//   "success": false,
	//   "error": "unexpected end of JSON input"
	// }

	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	type container struct {
		Success bool   `json:"success"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	err = json.Unmarshal(bbs, &cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return err
	}

	return err
}

// GetSwaps returns swap status data.
func (a API) GetSwaps() ([]Swap, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}

	// prepare the request
	uri := fmt.Sprintf("%s/api/v1/swapstatus", a.DaemonURI)

	if a.Debug {
		log.Printf("uri = '%s'", uri)
	}

	// dont need to init new reader, just wasting of memory
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	// make the request
	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to get swaps, %v", err)
		log.Print(err)
		return nil, err
	}
	defer resp.Body.Close()

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseGetSwaps(bbs)
}

// parseGetSwaps parses the json returned by the request
// {
//   "success": true,
//   "data": [
//     {
//       "progress": 100,
//       "pair": [
//         "BTC",
//         "USDT"
//       ],
//       "time": 1579788821,
//       "lastEvent": 3,
//       "status": "Swapped",
//       "statusColor": "green",
//       "price": "91649999/7847775551",
//       "participant": false,
//       "action": "INIT/SUCCESS",
//       "error": "",
//       "orderAction": 0,
//       "id": "orderId_c6160f89-87f1-4eae-9e23-f083ad373e7a",
//       "progressState": {
//				...
//       },
//       "leadCurrencyConfirmations": 1,
//       "oppositeCurrencyConfirmations": 2,
//       "nextRetry": "0001-01-01T00:00:00Z"
//     }
//   ],
//   "balance": "1"
// }
//
func parseGetSwaps(body []byte) ([]Swap, error) {
	// define the structs we need to parse the json
	type data struct {
		Progress       float64  `json:"progress"`
		Pair           []string `json:"pair"`
		Time           int64    `json:"time"`
		Status         string   `json:"status"`
		Price          string   `json:"price"`
		Participant    bool     `json:"participant"`
		Action         string   `json:"action"`
		OrderID        string   `json:"id"`
		BConfirmations int      `json:"leadCurrencyConfirmations"`
		QConfirmations int      `json:"oppositeCurrencyConfirmations"`
	}
	type container struct {
		Success bool   `json:"success"`
		Data    []data `json:"data"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	// don't need to initialize new reader, there more optimal func
	// err := json.NewDecoder(bytes.NewReader(body)).Decode(&cr)
	err := json.Unmarshal(body, &cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return nil, err
	}

	swaps := make([]Swap, len(cr.Data))
	for i, d := range cr.Data {
		swaps[i].Progress = d.Progress
		swaps[i].Pair = fmt.Sprintf("%s/%s", d.Pair[0], d.Pair[1])
		swaps[i].Time = time.Unix(d.Time, 0).UTC()
		swaps[i].Status = d.Status
		swaps[i].Quantities = d.Price
		swaps[i].Participant = d.Participant
		swaps[i].Action = d.Action
		swaps[i].OrderID = d.OrderID
		swaps[i].BConfirmations = d.BConfirmations
		swaps[i].QConfirmations = d.QConfirmations
	}

	return swaps, nil
}

// Version
func (a API) Version() (*Version, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}
	var uri, ps string

	uri = fmt.Sprintf("%s/api/v1/balance", a.DaemonURI)

	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(ps))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to get balance: %v", err)
		log.Print(err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseVersion(bbs)
}

func parseVersion(body []byte) (*Version, error) {
	// define the struct we need to parse the json
	type data struct {
		Build   string `json:"build"`
		Version string `json:"balance"`
	}
	type container struct {
		Success bool   `json:"success"`
		Data    data   `json:"data"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return nil, err
	}

	versionResponse := &Version{
		Build:   cr.Data.Build,
		Version: cr.Data.Version,
	}

	return versionResponse, nil
}

// GetLicenseKey
func (a API) GetLicenseKey() (*string, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}
	var uri, ps string

	uri = fmt.Sprintf("%s/api/v1/license/get", a.DaemonURI)

	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(ps))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	resp, err := c.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to get license key: %v", err)
		log.Print(err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseGetLicenseKey(bbs)
}

func parseGetLicenseKey(body []byte) (*string, error) {
	// define the struct we need to parse the json
	type container struct {
		Success bool   `json:"success"`
		Data    string `json:"data"`
		Error   string `json:"error"`
	}
	var cr container

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&cr)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	if !cr.Success {
		err = fmt.Errorf("server failure, %s", cr.Error)
		log.Print(err)
		return nil, err
	}

	return &cr.Data, nil
}