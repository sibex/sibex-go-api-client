package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func (a API) makeRequest(method string, url string, body io.Reader) (*http.Response, error) {
	var c = &http.Client{Timeout: a.Timeout * time.Second}

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if a.User != "" {
		req.SetBasicAuth(a.User, a.Password)
	}

	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// GetBalanceV2 get user balance by given currency.
func (a API) GetBalanceV2(cur Currency) (*Balance, error) {
	url := fmt.Sprintf("%s/api/v2/%s/balance", a.DaemonURI, cur.String())

	resp, err := a.makeRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseGetBalance(cur, bbs)
}

// BackupBTCWallet exports the wallet.dat Bitcoin wallet to a file. This only works with BTC nodes who are hosted on the same machine as the daemon
func (a API) BackupBTCWallet(path string) error {

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/btc/backup", a.DaemonURI), strings.NewReader(path))
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/btc/backup", a.DaemonURI))
		log.Printf("params = '%s'", path)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	switch resp.StatusCode {
	case http.StatusOK:
		return nil
	case http.StatusBadRequest:
		return errors.New("The wallet is not on the same node as the daemon")
	case http.StatusServiceUnavailable:
		return errors.New("Bitcoin node not connected")
	}

	return nil
}

// GetBTCBalance returns the balance of the connected Bitcoin wallet in satoshi.
func (a API) GetBTCBalance() (*Balance, error) {

	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/btc/balance", a.DaemonURI), nil)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusServiceUnavailable {
		return nil, errors.New("Bitcoin node not connected")
	}

	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseBTCBalance(bbs)
}

func parseBTCBalance(body []byte) (*Balance, error) {
	type response struct {
		Address string `json:"address"`
		Balance int64  `json:"balance"`
	}

	var resp response
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&resp)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	return &Balance{
		Currency:  Btc,
		Address:   resp.Address,
		Balance:   &BigInt{(big.NewInt(resp.Balance))},
		Available: true,
	}, nil
}

// GetBTCAddressForDeposit get a BTC address on which funds can be deposited onto.
// Address might change when endpoint is called multiple times.
func (a API) GetBTCAddressForDeposit() (string, error) {

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/btc/depositaddress", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusServiceUnavailable {
		return "", errors.New("Bitcoin node not connected")
	}

	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetBTCFee returns the calculated Bitcoin miner fee
func (a API) GetBTCFee(txSize string) (*Balance, error) {

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/btc/fee", a.DaemonURI), strings.NewReader(txSize))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusServiceUnavailable {
		return nil, errors.New("Bitcoin node not connected")
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseBTCBalance(bbs)
}

// BTCInfo DTO for get btc info
type BTCInfo struct {
	// Info are main or test
	Info string `json:"info"`
	// SyncProgress - number between 0 and 1
	SyncProgress float64 `json:"syncProgress"`
	// Network is human readable Bitcoin network name, e.g. 'Mainnet', 'Testnet'
	Network string `json:"network"`
	// URL of the Bitcoin JSON RPC, e.g. 'localhost:8332'
	URL string `json:"url"`
}

// GetBTCInfo get information about bitcoin node
func (a API) GetBTCInfo() (*BTCInfo, error) {

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/btc/info", a.DaemonURI), nil)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/btc/info", a.DaemonURI))
	}

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseGetBTCInfo(bbs)
}

func parseGetBTCInfo(body []byte) (*BTCInfo, error) {
	var btcInfo BTCInfo

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&btcInfo)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	return &btcInfo, nil
}

// GetBTCTradingEquity returns BTC trading equity - this is your balance minus all the money you have in open orders.
func (a API) GetBTCTradingEquity() (float64, error) {

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/btc/tradingequity", a.DaemonURI), nil)
	if err != nil {
		return 0, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/btc/tradingequity", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	tradingEquity, err := strconv.ParseFloat(string(bbs), 64)
	if err != nil {
		return 0, err
	}

	return tradingEquity, nil
}

// WithdrawBTC withdraws BTC funds to an external address.
func (a API) WithdrawBTC(to, value string) (string, error) {
	body := fmt.Sprintf(`{"to": "%s", "value": "%s"}`, to, value)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/btc/withdraw", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/btc/withdraw", a.DaemonURI))
		log.Printf("params = '%s'", body)

	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return "", errors.New("Bitcoin node not connected")
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// CommserverCancelOrder action to cancel an order.
func (a API) CommserverCancelOrder(orderID string) (string, error) {
	body := fmt.Sprintf(`{"orderId": "%s"}`, orderID)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/cancelorder", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusForbidden:
		return "", errors.New("You don't own this order")
	case http.StatusNotFound:
		return "", errors.New("No order with this ID found. Note that orders that have already matched cannot be canceled anymore and are under Swap status.")
	case http.StatusServiceUnavailable:
		return "", errors.New("Commserver is not connected.")
	}

	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetCommserverInfo get info about commserver
func (a API) GetCommserverInfo() (string, error) {

	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/commserver/info", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusServiceUnavailable {
		return "", errors.New("Commserver is not connected.")
	}

	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// CommserverLicense DTO for set license.
type CommserverLicense struct {
	Owner struct {
		FirstName    string `json:"firstName"`
		LastName     string `json:"lastName"`
		Organization string `json:"organization"`
		Email        string `json:"email"`
	} `json:"owner"`
	Valid bool   `json:"valid"`
	Error string `json:"error"`
}

// SetCommserverLicense set the license key for use with the commserver
//
//This endpoint will return 200 in case the license is invalid. For this endpoint, check the 'valid' attribute in the response and act according to the error code.
func (a API) SetCommserverLicense(key string) (*CommserverLicense, error) {

	body := fmt.Sprintf(`{"key": "%s"}`, key)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/license", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusServiceUnavailable {
		return nil, errors.New("Commserver is not connected.")
	}

	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseCommserverLicense(bbs)
}

func parseCommserverLicense(body []byte) (*CommserverLicense, error) {
	var commserverLicense CommserverLicense

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&commserverLicense)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	return &commserverLicense, nil
}

// GetCommserverMarket get market name, or null, given two currencies
func (a API) GetCommserverMarket(pair Pair) (string, error) {

	body := fmt.Sprintf(`{"currency1":"%s", "currency2": "%s"}`, pair.Base.String(), pair.Quote.String())

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/market", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/market", a.DaemonURI))
		log.Printf("params = '%s'", body)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return "", errors.New("Commserver is not connected.")
	}

	return string(bbs), nil
}

// MyOrderV2 DTO for order entity.
type MyOrderV2 struct {
	Created     string `json:"created"`
	Dark        bool   `json:"dark"`
	OrderID     string `json:"orderId"`
	OrderMarket string `json:"orderMarket"`
	Quantities  string `json:"quantities"`
}

// MyOrdersV2 ful response from order list request.
type MyOrdersV2 struct {
	OrderType                string      `json:"orderType"`
	Market                   string      `json:"market"`
	BuyOrders                []MyOrderV2 `json:"buyOrders"`
	SellOrders               []MyOrderV2 `json:"sellOrders"`
	LeadCurrencyExponent     float64     `json:"leadCurrencyExponent"`
	OppositeCurrencyExponent float64     `json:"oppositeCurrencyExponent"`
}

// GetMyOrdersV2 get your orders in the commbook
func (a API) GetMyOrdersV2() (*MyOrdersV2, error) {

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/myorders", a.DaemonURI), nil)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/market", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return nil, errors.New("Commserver is not connected.")
	}

	return parseMyOrdersV2(bbs)
}

func parseMyOrdersV2(body []byte) (*MyOrdersV2, error) {
	var orders MyOrdersV2

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&orders)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	return &orders, nil
}

// OrdersV2 DTO for general orders in commbook.
type OrdersV2 struct {
	OrderType                string    `json:"orderType"`
	Market                   string    `json:"market"`
	BuyOrders                []OrderV2 `json:"buyOrders"`
	SellOrders               []OrderV2 `json:"sellOrders"`
	LeadCurrencyExponent     float64   `json:"leadCurrencyExponent"`
	OppositeCurrencyExponent float64   `json:"oppositeCurrencyExponent"`
}

// OrderV2 order details.
type OrderV2 struct {
	Quantities string `json:"quantities"`
	OrderID    string `json:"orderId"`
}

// GetOrdersV2 get orders in the commbook.
func (a API) GetOrdersV2(pair Pair) (*OrdersV2, error) {

	body := fmt.Sprintf(`{"currency1": "%s", "currency2": "%s"}`, pair.Base.String(), pair.Quote.String())

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/orders", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/orders", a.DaemonURI))
		log.Printf("params = '%s'", body)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return nil, errors.New("Commserver is not connected.")
	}

	return parseOrdersV2(bbs)
}

func parseOrdersV2(body []byte) (*OrdersV2, error) {
	var orders OrdersV2

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&orders)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	return &orders, nil
}

// GetPairsV2 get the markets which currently have orders in them.
func (a API) GetPairsV2() (string, error) {

	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/commserver/pairs", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/pairs", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return "", errors.New("Commserver is not connected.")
	}

	return string(bbs), nil
}

// PlaceOrder request for placing order flow.
type PlaceOrder struct {
	Market      string `json:"market"`
	OrderAction int8   `json:"orderAction"`
	Quantities  string `json:"quantities"`
	Dark        bool   `json:"dark"`
	ValidUntil  string `json:"validUntil"`
}

// PlaceOrderResult response from place order request.
type PlaceOrderResult struct {
	OrderID            string `json:"orderId"`
	IsPlacedToCommbook bool `json:"isPlacedToCommbook"`
}

type PlaceOrderResultWrapped struct {
	Success			bool `json:"success"`
	Data			PlaceOrderResult `json:"data"`
	Version     	string `json:"version"`
}

// PlaceOrderV2 action to place an order.
func (a API) PlaceOrderV2(order PlaceOrder) (*PlaceOrderResult, error) {
	body, err := json.Marshal(order)
	if err != nil {
		return nil, err
	}

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/placeorder", a.DaemonURI), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/placeorder", a.DaemonURI))
		log.Printf("params = '%s'", string(body))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return nil, errors.New("Commserver is not connected.")
	}

	return parsePlaceOrderV2(bbs)
}

func parsePlaceOrderV2(body []byte) (*PlaceOrderResult, error) {
	var res PlaceOrderResultWrapped

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&res)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	return &res.Data, nil
}

// PriceV2 price histor entity.
type PriceV2 struct {
	// why the first letter is capital?
	Pair       string `json:"Pair"`
	Quantities string `json:"quantities"`
}

// GetPriceV2 get the price for a pair.
func (a API) GetPriceV2(pair Pair) (*PriceV2, error) {

	body := fmt.Sprintf(`{"currency1": "%s", "currency2": "%s"}`, pair.Base.String(), pair.Quote.String())

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/price", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/price", a.DaemonURI))
		log.Printf("params = '%s'", body)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	switch resp.StatusCode {
	case http.StatusNotFound:
		return nil, errors.New("No orders matched for this pair.")
	case http.StatusServiceUnavailable:
		return nil, errors.New("Commserver is not connected.")
	}

	return parsePriceV2(bbs)
}

func parsePriceV2(body []byte) (*PriceV2, error) {
	var price PriceV2

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&price)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	return &price, nil
}

// GetPriceHistoryV2 get the price history for a pair.
func (a API) GetPriceHistoryV2(pair Pair) (*GetPriceHistoryResponseDataV2, error) {

	body := fmt.Sprintf(`{"currency1": "%s", "currency2": "%s"}`, pair.Base.String(), pair.Quote.String())

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/price", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/price/history", a.DaemonURI))
		log.Printf("params = '%s'", body)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return nil, errors.New("Commserver is not connected.")
	}

	return parsePriceHistoryV2(bbs)
}

func parsePriceHistoryV2(body []byte) (*GetPriceHistoryResponseDataV2, error) {
	var ph GetPriceHistoryResponseDataV2

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&ph)
	if err != nil {
		log.Print("Failed to decode response body")
		return nil, err
	}

	return &ph, nil
}

// PricePoint is a trade that happened at some point.
type PricePointV2 struct {
	PriceDeprecated int64     `json:"price"`
	Quantities      int64     `json:"quantities"`
	Time            time.Time `json:"time"`
}

// GetPriceHistoryResponseDataV2 response for price history.
type GetPriceHistoryResponseDataV2 struct {
	Pair   string
	Prices []PricePointV2 `json:"prices"`
}

// UnsetLicenseV2 remove the license used with the commserver.
func (a API) UnsetLicenseV2() error {

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/unsetlicense", a.DaemonURI), nil)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/unsetlicense", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return errors.New("Commserver is not connected.")
	}

	return nil
}

// GetCommserverVersionV2 get balance information from commserver
//
//Get SIBEX Commserver build and balance information
func (a API) GetCommserverVersionV2() (string, string, error) {

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/commserver/balance", a.DaemonURI), nil)
	if err != nil {
		return "", "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/commserver/balance", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	if resp.StatusCode == http.StatusServiceUnavailable {
		return "", "", errors.New("Commserver is not connected.")
	}

	return parseCommserverVersionV2(bbs)
}

func parseCommserverVersionV2(body []byte) (string, string, error) {
	type version struct {
		Build   string `json:"build"`
		Version string `json:"version"`
	}

	var v version

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&v)
	if err != nil {
		log.Print("Failed to decode response body")
		return "", "", err
	}

	return v.Build, v.Version, nil
}

// GetERC20BalanceV2 returns the balance of an ERC20 token of the connected ETH wallet.
func (a API) GetERC20BalanceV2() (string, float64, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/erc20/balance", a.DaemonURI), nil)
	if err != nil {
		return "", 0, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/erc20/balance", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", 0, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseERC20BalanceV2(bbs)
}

func parseERC20BalanceV2(body []byte) (string, float64, error) {
	type balance struct {
		Address string  `json:"address"`
		Balance float64 `json:"balance"`
	}

	var ercBalance balance

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&ercBalance)
	if err != nil {
		log.Print("Failed to decode response body")
		return "", 0, err
	}

	return ercBalance.Address, ercBalance.Balance, nil
}

// GetERC20DepositAddressV2 get a ERC20 deposit address.
// Address might change when endpoint is called multiple times.
func (a API) GetERC20DepositAddressV2() (string, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/erc20/depositaddress", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/erc20/depositaddress", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetERC20Fee get ERC20 miner fee
// Returns the calculated ERC20 miner fee
func (a API) GetERC20FeeV2(token string, amount *big.Int) (string, error) {

	body := fmt.Sprintf(`{"token": "%s", "value": "%s"}`, token, amount.String())

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/erc20/fee", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/erc20/fee", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetERC20TradingEquity get ERC20 trading equity
// Returns your balance minus equity of open orders
func (a API) GetERC20TradingEquityV2() (string, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/erc20/tradingequity", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/erc20/tradingequity", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// WithdrawERC20 withdraws ETH funds to an external address
func (a API) WithdrawERC20V2(to, value string) (string, error) {

	body := fmt.Sprintf(`{"to": "%s", "value": "%s"}`, to, value)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/erc20/withdraw", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/erc20/withdraw", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetETHAddress get the addresses of the ETH wallet
// But in sibex daemon it returns only one address: sibex/daemon/eth.go:306L
func (a API) GetETHAddressV2() (string, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/eth/addresses", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/eth/addresses", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// MakeETHBackup makes a backup of the ETH node
// Stores ETH backup to a local file. Only works with ETH nodes which are on the same computer.
func (a API) MakeETHBackupV2(path string) (string, error) {
	body := fmt.Sprintf(`{"path": "%s"`, path)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/eth/addresses", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/eth/addresses", a.DaemonURI))
		log.Printf("params = '%s'", body)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetETHBalance returns the address and balance of the connected ETH wallet.
func (a API) GetETHBalanceV2() (string, float64, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/eth/addresses", a.DaemonURI), nil)
	if err != nil {
		return "", 0, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/eth/addresses", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", 0, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseETHBalance(bbs)
}

func parseETHBalance(body []byte) (string, float64, error) {
	type balance struct {
		Address string  `json:"address"`
		Balance float64 `json:"balance"`
	}

	var b balance

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&b)
	if err != nil {
		log.Print("failed to decode response body")
		return "", 0, err
	}

	return b.Address, b.Balance, nil
}

// GetETHDepositAddressV2
func (a API) GetETHDepositAddressV2() (string, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/eth/depositaddress", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/eth/depositaddress", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetETHFee returns the calculated ETH miner fee
func (a API) GetETHFeeV2(amount string) (float64, error) {
	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/eth/fee", a.DaemonURI), strings.NewReader(amount))
	if err != nil {
		return 0, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/eth/fee", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	fee, err := strconv.ParseFloat(string(bbs), 64)
	if err != nil {
		return 0, err
	}

	return fee, nil
}

// ETHInfo information of the ETH network
type ETHInfo struct {
	// SyncProgress is blockchain synchronization progress - can be between 0 and 1.
	SyncProgress int8 `json:"syncProgress"`
	// Client name of the connected ETH client
	Client string `json:"client"`
	// Network is human readable Ethereum network name
	Network string `json:"network"`
	// URL of the Ethereum JSON RPC
	URL string `json:"url"`
	// Version number of the connected ETH node
	Version string `json:"version"`
}

// GetETHInfo returns info about ETH network, sync progress and URL of node connected.
func (a API) GetETHInfoV2() (*ETHInfo, error) {
	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/eth/info", a.DaemonURI), nil)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/eth/info", a.DaemonURI))
	}

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("server returned status: %v", resp.StatusCode)
		return nil, err
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseGetETHInfo(bbs)
}

func parseGetETHInfo(body []byte) (*ETHInfo, error) {
	var ethInfo ETHInfo

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&ethInfo)
	if err != nil {
		log.Print("failed to decode response body")
		return nil, err
	}

	return &ethInfo, nil
}

// GetETHTradingEquityV2 returns your balance minus equity of open orders
func (a API) GetETHTradingEquityV2() (string, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/eth/tradingequity", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/eth/tradingequity", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// WithdrawETHV2 withdraws ETH funds to an external address
func (a API) WithdrawETHV2(to, value string) (string, error) {

	body := fmt.Sprintf(`{"to": "%s", "value": "%s"}`, to, value)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/eth/withdraw", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/eth/withdraw", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetLicenseKeyV2 get the license key that is set
func (a API) GetLicenseKeyV2() (string, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/license/get", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/license/get", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// UnsetLicenseKeyV2 remove the license locally
func (a API) UnsetLicenseKeyV2(key string) (string, error) {
	body := fmt.Sprintf(`{"body": "%s"}`, key)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/license/unset", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/license/unset", a.DaemonURI))
		log.Printf("params = '%s'", body)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// SetLicenseKey set the license key locally
func (a API) SetLicenseKeyV2(key string) (string, error) {
	body := fmt.Sprintf(`{"body": "%s"}`, key)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/api/v2/license/set", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/license/set", a.DaemonURI))
		log.Printf("params = '%s'", body)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// SibexStatus info about full SIBEX system
type SibexStatus struct {
	// Commserver - whether commserver is connected
	Commserver bool `json:"commserver"`
	// CommserverError connection error of commserver if there is any, otherwise empty string
	CommserverError string `json:"commserverError"`
	// ETHConnected - whether ETH node is connected
	ETHConnected bool `json:"ethConnected"`
	// ETHError node connection error if there is any, otherwise empty string
	ETHError string `json:"ethError"`
	// BTCConnected - whether BTC node is connected
	BTCConnected bool `json:"btcConnected"`
	// BTCError node connection error if there is any, otherwise empty string
	BTCError string `json:"btcError"`
	// ETHurl ETH node URL that SIBEX is pointed to
	ETHurl string `json:"ethUrl"`
	// CommserverURL - URL that SIBEX is pointed to
	CommserverURL string `json:"commserverUrl"`
	// BTCurl node URL that SIBEX is pointed to
	BTCurl string `json:"btcUrl"`
	// MainnetMode - whether SIBEX is in mainnet mode. If not, just Testnet swaps are possible
	MainnetMode bool `json:"mainnetMode"`
}

// GetSibexStatusV2 get connection information from SIBEX
// See connection status of the nodes and error messages
func (a API) GetSibexStatusV2() (*SibexStatus, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/license/set", a.DaemonURI), nil)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/license/set", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseSibexStatus(bbs)
}

func parseSibexStatus(body []byte) (*SibexStatus, error) {
	var st SibexStatus

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&st)
	if err != nil {
		log.Print("failed to decode response body")
		return nil, err
	}

	return &st, nil
}

// GetSibexVersionV2 get version information
// Get SIBEX build and version information
func (a API) GetSibexVersionV2() (string, string, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/api/v2/version", a.DaemonURI), nil)
	if err != nil {
		return "", "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/api/v2/version", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return parseSibexVersion(bbs)
}

func parseSibexVersion(body []byte) (string, string, error) {
	type version struct {
		Build   string `json:"build"`
		Version string `json:"balance"`
	}

	var v version

	// parse the body as json
	err := json.NewDecoder(bytes.NewReader(body)).Decode(&v)
	if err != nil {
		log.Print("Failed to decode response body")
		return "", "", err
	}

	return v.Build, v.Version, nil
}

// ClearSwapStatusV2 available from Build 19.46.1 onwards. Makes it possible to clear swap statuses.
func (a API) ClearSwapStatusV2(dryRun bool) (string, error) {

	body := fmt.Sprintf(`{"dryRun": "%t"}`, dryRun)

	resp, err := a.makeRequest(http.MethodPost, fmt.Sprintf("%s/swapstatus/clear", a.DaemonURI), strings.NewReader(body))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/swapstatus/clear", a.DaemonURI))
		log.Printf("params = '%s'", body)
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}

// GetSwapStatusV2 get info about swap status
func (a API) GetSwapStatusV2() (string, error) {
	resp, err := a.makeRequest(http.MethodGet, fmt.Sprintf("%s/swapstatus", a.DaemonURI), nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if a.Debug {
		log.Printf("uri = '%s'", fmt.Sprintf("%s/swapstatus", a.DaemonURI))
	}

	// get the body
	bbs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if a.Debug {
		log.Printf("response = '%v'", string(bbs))
	}

	return string(bbs), nil
}
