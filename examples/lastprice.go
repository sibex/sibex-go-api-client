package main

import (
	"fmt"
	"log"
	"os"

	sapi "gitlab.com/sibex/sibex-go-api-client"
)

type EXConfig struct {
	APIUser    string
	APIPass    string
	DaemonHost string
	DaemonPort string
	SAPI       *sapi.API
}

func main() {
	ec := EXConfig{}
	var ok bool

	// daemon api user
	ec.APIUser, ok = os.LookupEnv("AUTH_API_USER")
	if !ok {
		log.Fatal("daemon API user not set")
	}

	// daemon api password
	ec.APIPass, ok = os.LookupEnv("AUTH_API_PASS")
	if !ok {
		log.Fatal("daemon API password not set")
	}

	// daemon host
	ec.DaemonHost, ok = os.LookupEnv("DAEMON_HOST")
	if !ok {
		log.Fatal("daemon host not set")
	}
	// daemon port
	ec.DaemonPort, ok = os.LookupEnv("DAEMON_PORT")
	if !ok {
		log.Fatal("daemon port not set")
	}
	apiURI := fmt.Sprintf("http://%s:%s", ec.DaemonHost, ec.DaemonPort)
	cfg := sapi.Config{
		DaemonURI: apiURI,
		User:      ec.APIUser,
		Password:  ec.APIPass,
	}

	var err error
	ec.SAPI, err = sapi.NewAPI(cfg)
	if err != nil {
		log.Panicf("failed to instantiate API, %v", err)
	}

	prs := []sapi.Pair{
		{Base: sapi.Btc, Quote: sapi.Eth},
		{Base: sapi.Btc, Quote: sapi.USDT},
		{Base: sapi.Eth, Quote: sapi.USDT},
	}

	for _, pr := range prs {
		lp, err := ec.SAPI.GetLastPrice(pr)

		if err != nil {
			log.Panicf("failed to get last price, %v", err)
		}
		if lp != nil {
			log.Printf("last price for %s/%s: %v", pr.Base, pr.Quote, lp)
		} else {
			log.Printf("no last price available for %s/%s", pr.Base, pr.Quote)
		}
	}
}
